#+title: Screw it, let's to it. Richard Branson
#+filetags: book, review

* Bookmarks
  - Page 29: What is the point of hiring bright people (1) If you don't listen
    to them (2) Don't use their talent.
  - Page 67: Now is important.
  - Page 75: Money is for making things happen.
  - Page 76: The man in the street often has more common sense than many big bosses.
  - Page 83: Respect (def.)
