#+title: [Notes] Handy commands (Command that works!)
#+date: <2016-10-09>
#+filetags: gs ssh wget mplayer ffmpeg convert git
#+setupfile: ../org-templates/post.org
#+setupfile: ../org-templates/latex-pdf.org
#+LaTeX_HEADER: \lhead{Handy commands}
#+LaTeX_HEADER: \usepackage[a4paper, landscape, margin=0.5in, top=1in, bottom=1in]{geometry}

List of commands that I find useful.

/(Last updated on Feb 15, 2020)/
** PDF tricks[fn:gs]
   - Reduce PDF size
     #+BEGIN_SRC shell
       gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -sOutputFile=new_file.pdf original_file.pdf
       # Also try using different 'dPDFSETTINGS' settings explained in http://milan.kupcevic.net/ghostscript-ps-pdf/, see what works for you.
     #+END_SRC

   - Unlock PDF file
     #+BEGIN_SRC shell
       gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -SPDFPassword=THE_PASSWORD -sOutputFile=unencrypted.pdf -c .setpdfwrite -f encrypted.pdf
     #+END_SRC

   - Extract pages from PDF file
     #+BEGIN_SRC shell -n
       # Extract pages 1 to 3 from a file.
       gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=3 -sOutputFile=out_file_page_1_to_3.pdf input_file.pdf

       # Or extract pages from 3 to 7
       gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sPageList=3-7 -sOutputFile=out_file.pdf in_file.pdf

       # Extract page 1,2,3,4, & 6
       gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sPageList=1-4,6 -sOutputFile=out_file.pdf in_file.pdf

       # Extract page 1,4,5,6 & 9
       gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sPageList=1,4,5,6,9 -sOutputFile=out_file.pdf in_file.pdf
     #+END_SRC

   - Merge PDF files
     #+BEGIN_SRC shell
       gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=out_file.pdf in_file_1.pdf in_file_2.pdf
     #+END_SRC

** Media file tricks
   - Rotate video
      #+BEGIN_SRC shell -n
        # Using mplayer(watch rotated video)
        mplayer -vf-add rotate=1 sample.mp4

        # Using ffmpeg
        ffmpeg -i infile.mp4 -strict -2 -vf "transpose=1" outfile.mp4
      #+END_SRC

   - Convert 3GP to MP4
      #+BEGIN_SRC shell
	ffmpeg -i VID_0050.3gp -strict -2 -q:a 0 -ab 64k -ar 44100 VID_0050.mp4
      #+END_SRC
   - FLV to MP4
      #+BEGIN_SRC shell
	ffmpeg -i input.flv -qscale 1 -ar 22050 output.mp4
      #+END_SRC

   - MKV to MP4
     #+BEGIN_SRC shell -n
       ffmpeg -i input.mkv -c:v mpeg4 -c:a libmp3lame -b:a 128K output.mp4
       # or you can keep the video stream
       ffmpeg -i input.mkv -c:v copy -c:a aac -b:a 128K output.mp4

       # Convert initial 10 seconds of the video for testing
       ffmpeg -t 10 -i input.mkv -c:v copy -c:a aac -b:a 128K output.mp4
     #+END_SRC

   - [[https://manuals.playstation.net/document/en/ps4/videos/mp_format_v.html][PS4 compatible format]] [fn:freenode]
     #+BEGIN_SRC shell -n
       # Ex1
       ffmpeg -i input.mkv -c:v libx264 -c:a libmp3lame -preset slow -profile:v high -level 4.2 -b:a 320K output.mkv
       # Ex2
       ffmpeg -i input.mkv -c:v libx264 -vf format=yuv420p -c:a libmp3lame -preset slow -profile:v high -level 4.2 -b:a 320K output.mkv
       # Ex3
       ffmpeg -i input.mkv -c:v libx264 -vf format=yuv420p -c:a libmp3lame -preset slow -profile:v high -level 4.2 -crf 21 -b:a 320K output.mkv
       # Ex4
       ffmpeg -i input.mkv -c:v libx264 -vf format=yuv420p -c:a aac -preset slow -profile:v high -level 4.2 -crf 21 -b:a 128K output.mkv
     #+END_SRC

     Refer [[./ffmpeg_libmp3lame.html][this post]] to compile =ffmpeg= with lame & libx264 support.

   - Extract audio(MP3) from MP4 video
      #+BEGIN_SRC shell -n
        ffmpeg -i video.mp4 -f mp3 -ab 192000 -vn music.mp3

        # or specify codec to use `libmp3lame` in this case.
        ffmpeg -i video.mp4 -f mp3 -codec:a libmp3lame -ab 320000 -vn music.mp3
      #+END_SRC

      /where:/

      *-i*: input file

      *-f mp3*: file format should be MP3

      *-ab 192000*: audio should be encoded at 192Kbps. 320000 for 320Kbps

      *-vn*: don't want video

   - Merge Audio:
     #+BEGIN_SRC bash -n
       # Assuming that the video does not contain any audio stream.
       ffmpeg -i in_video.mp4 -i in_audio.mp3 -c:v copy -c:a mp3 output.mp4
     #+END_SRC

     /where:/

     *-c:v copy*: Copy the audio stream. Similar to *-vcodec copy*

     *-c:a*: Set MP3 code

   - Cut video starting from 19 min 49 seconds up to 04 mins 18 seconds.
      #+BEGIN_SRC shell
        ffmpeg -y -i Video.mp4 -qscale 1 -ss 00:19:49.0 -t 00:04:18.0 -acodec copy -vcodec copy output.mp4
      #+END_SRC

      /Note/: *-sameq* was removed in the recent versions of =ffmpeg=

   - Use =mplayer= to extract audio(MP3)
     #+BEGIN_SRC shell
       mplayer -dumpaudio movie.flv -dumpfile movie_audio_track.mp3
     #+END_SRC
   - Reduce resolution
     #+BEGIN_SRC shell
       ffmpeg -i Birdman.mp4 -strict -2  -s 720x480 birdman.mp4
     #+END_SRC
   - Record a screen cast using *recordmydesktop*
     #+BEGIN_SRC shell
       recordmydesktop -x 100 -y 100 --width 1280 --height 720 --freq 48000 --fps 30 -o output.ogv
     #+END_SRC
   - Record a screen cast using *ffmpeg* ([[https://wiki.archlinux.org/index.php/GDM#GDM_ignore_Wayland_and_use_X.Org_by_default][Please disable Wayland]]):
     #+BEGIN_SRC bash
       ffmpeg -video_size 1280x720 -show_region 1 -framerate 30 -f x11grab -i :1+30,30 output.mp4
     #+END_SRC

     /where:/

     =-video_size 1280x720= Width and height of 1280x720

     =-show_region 1= Highlight a region to be recorded

     =-framerate 30= 30 FPS

     =-i :1+30,30= On DISPLAY :1 starting with the upper-left corner at x=30, y=30

   - Include subtitles(Use =subtitleeditor= to create subtitle)
     #+BEGIN_SRC shell
       ffmpeg -i video.avi -vf subtitles=subtitle.srt out.avi
     #+END_SRC

   - Adjust aspect ratio
     #+BEGIN_SRC shell
       ffmpeg -i input.mp4 -aspect 1280:720 -c copy output.mp4
       ffmpeg -i input.mp4 -vf scale=1280:720 -c:v h264 output.mp4 # Using h264 video codec
     #+END_SRC

** Image manipulation
   - Capture screenshot:
     #+BEGIN_SRC shell
       import -window root screenshot.png
     #+END_SRC
   - Re-size image 50% of its original size:
     #+BEGIN_SRC shell
       convert dragon.gif -resize 50% half_dragon.gif
     #+END_SRC
   - Limit pixel count to 30000:
     #+BEGIN_SRC shell
       convert input.png -resize 3000@ output.png
     #+END_SRC
   - Limit maximum image size of 265x64(hxw):
     #+BEGIN_SRC shell
       convert input.png -resize '265x64' output.png
     #+END_SRC
   - Limit image size of 265x64(hxw):
     #+BEGIN_SRC shell
       convert input.png -resize '265x64!' output.png
     #+END_SRC
   - Control compression level of an image:
     #+BEGIN_SRC shell
       convert input.png -quality 75 output.jpg
     #+END_SRC
   - Convert JPG(or any image) to PDF:
     #+BEGIN_SRC shell
       convert input.jpg -page a4 output.pdf
     #+END_SRC
   - Combine images
     #+BEGIN_SRC shell
       convert image_1.jpg image_2.jpg -append out.jpg
       # Note: '-' will combine images vertically whereas '+' will combine images horizontally
     #+END_SRC
   - Crop image
     #+CAPTION: Crop
     #+ATTR_HTML: :class center
     #+ATTR_HTML: :width 100% :height
     file:images/posts/handy_commands/crop.svg

     #+BEGIN_SRC shell
       convert -crop 2560x1440+10+10 in.png out.png
     #+END_SRC

** Git[fn:lwn_git_sha_256]
   - Find commits by author between the time-line
     #+BEGIN_SRC shell -n
       # Syntax
       git log --since "START" --until "END" --author=AUTHOR --pretty=oneline --diff-filter=A

       # Example
       git log --since "Oct 1 2018" --until "DEC 28 2018" --author=psachin@redhat.com --pretty=oneline --diff-filter=A

       # If you also want to see the file names
       git log --since "Oct 1 2018" --until "DEC 28 2018" --author=psachin@redhat.com --name-status --pretty=oneline --diff-filter=A
     #+END_SRC
   - Clone specific branch from remote
     #+BEGIN_SRC shell -n
       git clone -b <REMOTE_BRANCH> <REMOTE_REPO_URL>
       # Example
       git clone -b ics https://github.com/androportal/abt.git
     #+END_SRC
   - Checkout specific branch
     #+BEGIN_SRC shell -n
       ## New
       git fetch <REMOTE>
       get branch --all  # show all the branches
       git checkout <BRANCH>

       ## Old
       git checkout -b <REMOTE_BRANCH> remotes/<REMOTE>/<BRANCH>
       # Example
       git checkout -b holo_theme remotes/origin/holo_theme
     #+END_SRC
   - Squash
     #+BEGIN_SRC shell -n
       # Squash last five commits interactively
       git rebase -i HEAD~5  # and follow the instructions
     #+END_SRC

** Misc
   - Clean up =~/.ccache/= directory
     #+BEGIN_SRC bash -n
       # View statistics using
       ccache -s

       # Clear cache using
       ccache -C
     #+END_SRC

   - wget a website
     #+BEGIN_SRC bash
       wget -rkp -l5 -np -nH --cut-dirs=1 https://example.com
     #+END_SRC

     /where:/

     *-rkp*: recursive, make link suitable for local viewing, download all files needed to properly view the page.

     *-l5*: recursively download 5 links away form the original page.

     *-n*: retrieve files below parent directory.

     *-H*: Span across hosts when doing recursive retrieving.

   - Find the File system/partition of current directory
     #+BEGIN_SRC bash
       df -h .   # Notice the DOT
     #+END_SRC

   - List commands/applications using IPv4/IPv6
     #+BEGIN_SRC bash -n
       lsof -i 4
       # and
       lsof -i 6

       # Sample output of `lsof -i 6`
       COMMAND     PID    USER   FD   TYPE  DEVICE SIZE/OFF NODE NAME
       emacs      3131 psachin   21u  IPv6 4519926      0t0  TCP nubia:55524->rajaniemi.freenode.net:ircu-3 (ESTABLISHED)
       emacs      3131 psachin   22u  IPv6 4524689      0t0  TCP nubia:41416->irc0.acc.umu.se:ircu-3 (ESTABLISHED)
       emacs      3131 psachin   23u  IPv6 4548798      0t0  TCP nubia:33452->strange.oftc.net:ircu-3 (ESTABLISHED)
       firefox    3656 psachin   51u  IPv6 5606852      0t0  TCP nubia:54302->sc-in-xbd.1e100.net:https (ESTABLISHED)
       chrome     3878 psachin  128u  IPv6 4518904      0t0  UDP *:mdns
       chrome     3878 psachin  148u  IPv6 4518906      0t0  UDP *:mdns
       thunderbi 13475 psachin   40u  IPv6 5552262      0t0  TCP nubia:35084->sc-in-x6d.1e100.net:imaps (ESTABLISHED)
       thunderbi 13475 psachin   61u  IPv6 4526804      0t0  TCP nubia:59268->sb-in-x6c.1e100.net:imaps (ESTABLISHED)
       thunderbi 13475 psachin   62u  IPv6 5592285      0t0  TCP nubia:60922->sa-in-x6d.1e100.net:imaps (ESTABLISHED)
       thunderbi 13475 psachin   85u  IPv6 5408348      0t0  TCP nubia:48110->sb-in-x6d.1e100.net:imaps (ESTABLISHED)
       thunderbi 13475 psachin  134u  IPv6 5561644      0t0  TCP nubia:44914->sb-in-x6c.1e100.net:imaps (ESTABLISHED)
     #+END_SRC

[fn:gs] *ghostscript* has a very good usage documentation at
https://www.ghostscript.com/doc/current/Use.htm
[fn:freenode] Special thanks to the guys at *#ffmpeg@freenode* for helping me
converting to the PS4 compatible format.
[fn:lwn_git_sha_256] https://lwn.net/Articles/811068/ (not related to the notes
mentioned in this post)
#+INCLUDE: "../disquss.inc"
